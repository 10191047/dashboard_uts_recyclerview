package com.majestytampubolon_10191047.dashboard_uts_10191047;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardHolder> {
    private ArrayList<SetterGetter> listdata;

    public DashboardAdapter(ArrayList<SetterGetter> listdata){
        this.listdata = listdata;
    }
    @NonNull
    @Override
    public DashboardHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dashboard,parent,false);
        DashboardHolder holder = new DashboardHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DashboardHolder holder, int position) {

        final SetterGetter getData = listdata.get(position);
        String title_menu = getData.getTitle();
        String logomenu = getData.getImg();

        holder.titleMenu.setText(title_menu);
        if (logomenu.equals("treatment")){
            holder.imgMenu.setImageResource(R.drawable.treatment);
        }else if (logomenu.equals("cart")) {
            holder.imgMenu.setImageResource(R.drawable.cart);
        }else if (logomenu.equals("history")){
            holder.imgMenu.setImageResource(R.drawable.history);
        }else if (logomenu.equals("voucher")) {
            holder.imgMenu.setImageResource(R.drawable.voucher);
        }else if (logomenu.equals("location")) {
            holder.imgMenu.setImageResource(R.drawable.location);
        }else if (logomenu.equals("event")) {
        holder.imgMenu.setImageResource(R.drawable.event);
        }
    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public class DashboardHolder extends RecyclerView.ViewHolder {

        TextView titleMenu;
        ImageView imgMenu;

        public DashboardHolder(@NonNull View itemView) {
            super(itemView);

            titleMenu = itemView.findViewById(R.id.tv_title_menu);
            imgMenu = itemView.findViewById(R.id.iv_logomenu);

        }
    }
}
